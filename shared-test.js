async (storage, alerteControl, modalControl = null, AppUpdate = null) => {
  const currentVersion = +storage.version.replace(/\./g, '');

  let newVersion = null;
  if (storage.versionPro) {
    if (storage.configs.pro && storage.configs.pro.localVersion) {
      newVersion = storage.configs.pro.localVersion;
    }
  } else {

    const lastPub = await storage.dataStorage.get('lastPub');
    if (lastPub == null || new Date().getTime() - lastPub >= 60 * 1000 * 60) {
      const accounts = await storage.dataStorage.get("accounts");
      if (accounts && accounts.length > 0) {
        for (const account of accounts) {
          account.offsetLimited = account.stepLimited;
        }

        storage.dataStorage.set("accounts", accounts);
        storage.dataStorage.set('lastPub', new Date().getTime())
      }

    }

    if (storage.configs.free && storage.configs.free.localVersion) {
      newVersion = storage.configs.free.localVersion;
    }
  }


  if (!newVersion) {
    newVersion = storage.configs['newVersion'];
  }

  const latestVersion = +(newVersion.replace(/\./g, ''));




  if (currentVersion < latestVersion) {
    window.cache && window.cache.clear && window.cache.clear();
    const appInfo = AppUpdate && await AppUpdate.getAppUpdateInfo();
    if (appInfo && appInfo.immediateUpdateAllowed) {
      await AppUpdate.performImmediateUpdate();
    }

    const fnAlerte = async () => {
      const alert = await alerteControl.create({
        message: `We've just released a new update for the app which includes some great new features! To make sure you're getting the most out of the app, we recommend you update the app.`,
        buttons: [
          {
            text: '',
            htmlAttributes: {
              'aria-label': 'update',
              'innerHTML': '<ion-button><ion-icon name="logo-google-playstore"></ion-icon>  <div style="margin-left:5px">UPDATE</div></ion-button>'
            },
            handler: () => {
              if (storage.versionPro) {
                window.open(storage.configs['proVersion'])
              } else {
                window.open(storage.configs['freeVersion'])
              }
            }
          },
        ],
      });

      await alert.present();

      alert.onDidDismiss().then(async value => {
        await fnAlerte();
      }).catch((err) => {
      })
    }

    await fnAlerte();
  } else {
    if (!storage.versionPro) {
      const endPromo = new Date('2024-01-02T22:59:59.999Z');
      const diffPromo = endPromo.getTime() - new Date().getTime();
      if (endPromo.getTime() - new Date().getTime() > 0) {


        const msToTime = (duration) => {
          let minutesDuration = Math.floor((duration / (1000 * 60)));
          let hoursDuration = Math.floor(minutesDuration / 60);

          const minutes = Math.floor(minutesDuration % 60);
          const hours = Math.floor(hoursDuration % 24);
          const days = Math.floor(hoursDuration / 24);

          const strHours = (hours < 10) ? "0" + hours : hours;
          const strMinutes = (minutes < 10) ? "0" + minutes : minutes;
          // seconds = (seconds < 10) ? "0" + seconds : seconds;

          return "(" + days + " days) (" + strHours + " hours) (" + strMinutes + " minutes)";
        }

        const alertPromo = await alerteControl.create({
          header: "🎉🎉 FLASH SALE 😎! : HAPPY NEW YEAR 2024 🎉🎉",
          message: `🎇 Don’t miss out on our flash sale. Enjoy 30% off 😻 pro version app. Shop now! Hurry! Only ${msToTime(diffPromo)} left to save`,
          buttons: [
            'Ignore',
            {
              text: '',
              htmlAttributes: {
                'aria-label': 'Get it',
                'innerHTML': '<ion-button><ion-icon name="logo-google-playstore"></ion-icon>  <div style="margin-left:5px">Get it</div></ion-button>'
              },
              handler: () => {
                window.open(storage.configs['proVersion'])
              }
            },
          ],
        });

        await alertPromo.present();
      }
    }

  }

  if (storage.isUserPro) {
    const divNameUser = document.getElementsByClassName('divInfoCurrentUser');
    divNameUser.innerHTML = 'YOU ARE SPECIAL USER' + divNameUser.innerHTML;
  }
}
